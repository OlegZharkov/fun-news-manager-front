# News Manager

News Manager

## Prerequisites

You would need node v9+ and angular CLI.
After you install node, just run 
`npm install -g @angular/cli`

## Install dependencies

`npm install`

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.


