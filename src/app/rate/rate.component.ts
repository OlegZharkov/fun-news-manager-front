import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar, MatTableDataSource} from '@angular/material';

/**
 * Component to show and rate imported news
 */
@Component({
    selector: 'app-rate',
    templateUrl: './rate.component.html',
    styleUrls: ['./rate.component.scss']
})
export class RateComponent implements OnInit {
    rest = window.location.origin;
    news: any;

    // Showed columns
    displayedColumns: string[] = ['date', 'creator', 'link', 'rate'];

    constructor(private http: HttpClient,
                private snackBar: MatSnackBar) {
    }

    ngOnInit() {
        // get news from REST API
        this.http.get<any>(this.rest + `/news-manager/rest/news`).subscribe(news => this.news = new MatTableDataSource(news), Error => {
            alert('getting news service failed');
        });
    }

    onRate(newsItem, rating) {
        // send put new user rating request
        this.http.put(this.rest + `/news-manager/rest/ratings/` + newsItem._id.$oid + '/' + rating, {})
            .subscribe(data => {
                console.log('Article successfully rated');
                this.snackBar.open('Article was successfully rated!', '', {
                    duration: 3000,
                    panelClass: 'center',
                    verticalPosition: 'top',
                    horizontalPosition: 'right'
                });
                newsItem.rate = 0;
            }, Error => {
                alert('rating failed');
                console.log(Error);
            });
    }

    getDate(newsItem) {
        // parses RSS date to more human friendly form
        const date = new Date(newsItem.pubDate);
        return  date.toDateString();
    }
}
