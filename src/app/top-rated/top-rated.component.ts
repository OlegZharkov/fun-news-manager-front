import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {toXML} from 'jstoxml';
import beautify from 'xml-beautifier';
import {ActivatedRoute} from '@angular/router';
import {interval, Observable, Subscription, timer} from 'rxjs';

/**
 * Component to show top rated values
 */
@Component({
    selector: 'app-top-rated',
    templateUrl: './top-rated.component.html',
    styleUrls: ['./top-rated.component.scss']
})
export class TopRatedComponent implements OnInit {
    rest = window.location.origin;
    ratings: any = {};
    xml: any;
    isXml: boolean;
    // do not wait on the first run
    currentUpdateInterval = 1;
    intervalRunner: Subscription;

    constructor(private http: HttpClient,
                private route: ActivatedRoute) {}

    ngOnInit() {
        // check if XML
        this.route.queryParams.subscribe(params => {
            // query string parameters are sent as string and here is the check if XML representation is enabled
            this.isXml = params.isXml.toLowerCase() === 'true';
        });
        // start cronjob
        this.initIntervalRunner(this.currentUpdateInterval);
    }

    /**
     * create a cronjob or update it with new interval value, which is equal to server
     * @param updateInterval
     */
    initIntervalRunner(updateInterval) {
        if (this.intervalRunner) {
            this.intervalRunner.unsubscribe();
        }
        const source = interval(updateInterval);
        this.intervalRunner = source.subscribe(val => this.update());
        this.currentUpdateInterval = updateInterval;
    }

    /**
     * Run update with every cycle of cronjob
     */
    update() {
        this.http.get<any>(this.rest + `/news-manager/rest/ratings/top-rated`).subscribe(response => {
            this.ratings = JSON.parse(response.ratings);
            // if update interval is different on backend, override current one with a new value
            if (response.updateInterval !== this.currentUpdateInterval) {
                this.initIntervalRunner(response.updateInterval);
            }
            /**
             * Show top rated news in XML, if user requested and also make it human-readable
             */
            if (this.isXml) {
                const xmlOptions = {
                    header: false,
                    indent: '  ',
                    filter: {
                        '<': '&lt;',
                        '>': '&gt;',
                        '"': '&quot;',
                        '\'': '&apos;',
                        '&': '&amp;',
                    }
                };
                this.xml = beautify(toXML(this.ratings, xmlOptions));
            }
        }, error => {
            alert('getting news service failed');
            console.error(error);
        });
    }
}
