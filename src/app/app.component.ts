import {Component, OnInit} from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';

/**
 * Top component, which is always present within application. Show top toolbar.
 */
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    ifHomeRoute: boolean;

    constructor(private router: Router) {
        // Detect current route
        router.events.subscribe(val => {
            if (val instanceof NavigationEnd) {
                this.ifHomeRoute = val.url === '/home' || val.url === '/';
            }
        });
    }
}
