import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {MatSelectChange, MatSnackBar} from '@angular/material';


@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss']
})
export class AdminComponent {
    rssUrl: string;
    rest = window.location.origin;
    panelOpenState: boolean;
    cronJobTimer;

    constructor(private http: HttpClient,
                private snackBar: MatSnackBar) {
    }

    /**
     * If the form is valid, sends import request of new URL Feed to REST API
     * @param form
     */
    addRss(form: NgForm) {
        if (form.valid) {
            this.http.post(this.rest + `/news-manager/rest/news`, {rssUrl: this.rssUrl})
                .subscribe(data => {
                    this.snackBar.open(this.rssUrl + ' news sucesfully imported!', '', {
                        duration: 3000,
                        panelClass: 'center',
                        verticalPosition: 'top',
                        horizontalPosition: 'right'
                    });
                }, Error => {
                    alert('rating failed');
                    console.log(Error);
                });
            this.panelOpenState = false;
            this.rssUrl = '';
        }
    }

    /**
     * Change the interval of top-rated news calculations.
     * @param $event
     */
    changeUpdateInterval($event: MatSelectChange) {
        console.log($event);
        this.http.post(this.rest + `/news-manager/rest/ratings/change-cron-interval/` + $event.value, {})
            .subscribe(data => {
                this.snackBar.open(' cron interval is updated!', '', {
                    duration: 3000,
                    panelClass: 'center',
                    verticalPosition: 'top',
                    horizontalPosition: 'right'
                });
            }, Error => {
                alert('rating failed');
                console.log(Error);
            });
    }
}
