import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RateComponent} from './rate/rate.component';
import {HomeComponent} from './home/home.component';
import {TopRatedComponent} from './top-rated/top-rated.component';
import {AdminComponent} from './admin/admin.component';

/**
 * A router wining Angular application
 */
const routes: Routes = [
  {path: 'rate', component: RateComponent},
  {path: 'home', component: HomeComponent},
  {path: 'top-rated', component: TopRatedComponent},
  {path: 'admin', component: AdminComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
