import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material-module';
import {RateComponent} from './rate/rate.component';
import {TopRatedComponent} from './top-rated/top-rated.component';
import {HomeComponent} from './home/home.component';
import {BarRatingModule} from 'ngx-bar-rating';
import {NgxJsonViewerModule} from 'ngx-json-viewer';
import { AdminComponent } from './admin/admin.component';

/**
 * Main module, contains import of dependencies and components initialization
 */
@NgModule({
    declarations: [
        AppComponent,
        RateComponent,
        TopRatedComponent,
        HomeComponent,
        AdminComponent,
    ],
    imports: [
        MaterialModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        BarRatingModule,
        NgxJsonViewerModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
